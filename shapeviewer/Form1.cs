﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace shapeviewer
{
    public partial class Form1 : Form
    {
        public string source = @"C:\Users\KBSE01\Documents\Shapefile\SHP_INPUT_Topology\Sungai di Lembangan Sungai Kerian.shp";
        public string source2 = @"C:\Users\KBSE01\Documents\Output Folder\[1515576838455] Sungai di Lembangan Sungai Kerian\_DOUBLE.shp";
        public string[] args = Environment.GetCommandLineArgs();


        public Form1()
        {
            InitializeComponent();

            SharpMap.Layers.VectorLayer vlay = new SharpMap.Layers.VectorLayer("Layer Name");
            SharpMap.Layers.VectorLayer vlay2 = new SharpMap.Layers.VectorLayer("Layer Name");

            try
            {
                this.Text = "SPDG Shapefile Viewer";
                Color[] arraycol = new Color[] { Color.Black, Color.Red, Color.Blue, Color.Green };
                //treeView1.Nodes.Add(Path.GetDirectoryName(args[1]), );
                for (int i = 1; i < args.Length; i++)
                {
                    var treenode = new TreeNode();
                    treenode.ForeColor = arraycol[i];
                    treenode.Tag = args[i];
                    treenode.Text = Path.GetFileName(args[i]);
                    treenode.Checked = true;
                    treeView1.Nodes.Add(treenode);

                    //Create the layer
                    var vectorlay = new SharpMap.Layers.VectorLayer("Layer Name");
                    //Assign the data source
                    vectorlay.DataSource = new SharpMap.Data.Providers.ShapeFile(args[i], true);
                    //Create the style
                    vectorlay.Style.Line = new Pen(arraycol[i], 1/2);

                    //Add layer to map
                    mapBox1.Map.Layers.Add(vectorlay);
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
                //vlay.DataSource = new SharpMap.Data.Providers.ShapeFile(source, true);
                //vlay2.DataSource = new SharpMap.Data.Providers.ShapeFile(source2, true);
                //this.Text = ".shp";

                //TreeNode tree = new TreeNode();
                //tree.Tag = source;
                //tree.Text = Path.GetFileName(source);
                //tree.Checked = true;
                //treeView1.Nodes.Add(tree);

                //TreeNode tree2 = new TreeNode();
                //tree2.Tag = source2;
                //tree2.Text = Path.GetFileName(source2);
                //tree2.Checked = true;
                //treeView1.Nodes.Add(tree2);

                
            }
            treeView1.TabStop = false;
            
            //mapBox1.Map.Layers.Add(vlay);
            //mapBox1.Map.Layers.Add(vlay2);
            mapBox1.Map.ZoomToExtents();
            mapBox1.Refresh();
            mapBox1.ActiveTool = SharpMap.Forms.MapBox.Tools.Pan;

            AppSetting app = new AppSetting();
            propertyGrid1.SelectedObject = app;
        }

        public class AppSetting
        {
            [CategoryAttribute("Properties"),
            ReadOnlyAttribute(true)]
            public string ShapeFileDirectory
            {
                get;
                set;
            }
            [CategoryAttribute("Properties"),
            ReadOnlyAttribute(true)]
            public string GeometryType
            {
                get;
                set;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
                ProcessStartInfo info = new ProcessStartInfo();
                info.FileName = "explorer";
                info.Arguments = string.Format("/e, /select, \"{0}\"", args[1]);

                Process.Start(info);
            }
            catch(Exception err)
            {
                MessageBox.Show(err.Message);
            }
            
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void treeView1_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if(e.Node.Checked == false)
            {
                MessageBox.Show(e.Node.Tag.ToString());
                //mapBox1.Map.Layers;
            }
            else
            {
                MessageBox.Show(true.ToString());
            }
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                AppSetting app = new AppSetting();
                app.ShapeFileDirectory = e.Node.Tag.ToString();
                propertyGrid1.SelectedObject = app;
            }
            catch (Exception error)
            { }

            //this.Text = e.Node.Text;
        }

        private void treeView1_DoubleClick(object sender, EventArgs e)
        {
            
        }

        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                ProcessStartInfo info = new ProcessStartInfo();
                info.FileName = "explorer";
                info.Arguments = string.Format("/e, /select, \"{0}\"", e.Node.Tag.ToString());

                Process.Start(info);
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }
    }
}
